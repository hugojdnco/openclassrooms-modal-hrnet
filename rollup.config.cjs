const styles = require('rollup-plugin-styles');
const autoprefixer = require('autoprefixer');
const { terser } = require('rollup-plugin-terser');
const babel = require('@rollup/plugin-babel');
const sourcemaps = require('rollup-plugin-sourcemaps');

// the entry point for the library
const input = 'src/index.js';

var MODE = [
    {
        format: 'cjs'
    },
    {
        format: 'esm'
    },
    {
        format: 'umd'
    }
];

var config = [];

MODE.map((m) => {
    var conf = {
        input: input,
        output: {
            // then name of your package
            name: "react-ocolek-modal",
            file: `dist/index.${m.format}.js`,
            format: m.format,
            exports: "auto"
        },
        // this externalizes react to prevent rollup from compiling it
        external: ["react", /@babel\/runtime/],
        plugins: [
            // these are babel configurations
            babel({
                exclude: 'node_modules/**',
                plugins: ['@babel/transform-runtime'],
                babelHelpers: 'runtime'
            }),
            // this adds sourcemaps
            sourcemaps(),
            // this adds support for styles
            styles({
                postcss: {
                    plugins: [
                        autoprefixer()
                    ]
                }
            })
        ]
    }
    config.push(conf)
});

module.exports = config;
