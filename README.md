
# Bienvenue dans holek_oc_modal_component 👋

<p align="center">
  <img src="https://img.shields.io/badge/version-npm%208.19.3-blue.svg" />
  <a href="https://gitlab.com/hugojdnco/openclassrooms-modal-hrnet">
    <img alt="Dépôt GitLab" src="https://img.shields.io/badge/dépôt-GitLab-orange" />
  </a>
  <a href="https://gitlab.com/hugojdnco/openclassrooms-modal-hrnet/blob/master/LICENSE">
    <img alt="Licence : BSD-3-Clause" src="https://img.shields.io/badge/licence-BSD--3--Clause-yellow.svg" />
  </a>
</p>

Composant modal pour React. Une manière aisée d'intégrer des modals dans vos projets React.

## 🚀 Installation

Pour ajouter ce composant modal à votre projet React, utilisez la commande suivante :

```
npm install holek_oc_modal_component
```

Ou si vous utilisez Yarn :

```
yarn add holek_oc_modal_component
```

## 📋 Prérequis

- Une application React déjà configurée et fonctionnelle.
- npm version 8.19.3 ou plus récente.

## 🔍 Utilisation

Après avoir satisfait aux prérequis, vous pouvez importer le composant et l'utiliser dans vos projets React comme suit :

```jsx
import Modal from 'holek_oc_modal_component';

function App() {
  const [modalVisible, setModalVisible] = React.useState(false);

  function hideModal() {
    setModalVisible(false);
  }

  return (
    <div>
      <Modal show={modalVisible} onClose={hideModal}>
        <p>Employé créé</p>
      </Modal>
      <button onClick={() => setModalVisible(true)}>Afficher le modal</button>
    </div>
  );
}

export default App;
```

## 🤝 Contributions

Les contributions, les issues et les demandes de fonctionnalités sont les bienvenues. Consultez la page des problèmes si vous souhaitez contribuer.

## Auteur

👤 **Hugo Olekhnovitch**

- Email: hugo@olek.fr
- GitLab: [@hugojdnco](https://gitlab.com/hugojdnco)

## Montrez votre soutien

Si ce projet vous a été utile, n'hésitez pas à lui donner une ⭐️ !

## 📝 Licence

Copyright © 2023 Hugo Olekhnovitch.<br />
Ce projet est sous licence BSD-3-Clause.
